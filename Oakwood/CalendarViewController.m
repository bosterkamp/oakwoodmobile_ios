//
//  CalendarViewController.m
//  Oakwood
//
//  Created by The Osterkamps on 11/20/12.
//  Copyright (c) 2012 Oakwood Baptist Church. All rights reserved.
//

#import "CalendarViewController.h"
//#import "CalendarDetails.h"
#import "EventDetails.h"
#import "ColorConverter.h"


@interface CalendarViewController ()

@end

@implementation CalendarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //NSLog(@"inside CalendarViewController");
    self.navigationItem.title = @"Calendar";
    
    [self.tableView setBackgroundColor: [ColorConverter colorFromHexString:@"#FFFFFF"]];
    [self.tableView setOpaque: NO];
    
    myParser = [[CalendarParser alloc] init];
    
    //Call the url to get the data
    //tableData = [myParser parseXMLFile:@"/Users/bosterkamp/Desktop/believe_dates.txt"];
    //tableData = [myParser parseXMLFile:@"/Users/bosterkamp/Desktop/mod_rrule_end_date.txt"];
    //tableData = [myParser parseXMLFile:@"/Users/bosterkamp/Desktop/mod_rrule_proper_end_date.txt"];
    
    
    //old way - need to migrate
    //tableData = [myParser parseXMLFile:@"http://oakwoodnb.com/calendars/all/feed/ical/"];
    
    //new way - need to fix
    tableData = [myParser parseXMLFile:@"http://oakwoodnb.com/events/ical/"];
    
    //NSLog(@"tableData size: %lu", (unsigned long)[tableData count]);
    
    if ([tableData count] == 0){
        //NSLog(@"table data is 0");
        UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,
                                                                        self.tableView.bounds.size.width,
                                                                        self.tableView.bounds.size.height)];
        //set the message
        messageLbl.text = @"Sorry, there are no calendar events available to display at this time.  Please check back later.";
        //center the text
        messageLbl.textAlignment = NSTextAlignmentCenter;
        messageLbl.numberOfLines = 0;
        //auto size the text
        [messageLbl sizeToFit];
        
        //set back to label view
        self.tableView.backgroundView = messageLbl;
        //no separator
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    /*
    else{
        NSLog(@"tableData size: %lu", (unsigned long)[tableData count]);
    }
     */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:simpleTableIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    EventDetails *ed = [tableData objectAtIndex:indexPath.row];
    
    //NSLog(@"Event %@", [cd eventSummary]);
    if ([ed eventDate] == NULL)
    {
        [ed setEventDate:@""];
    }
    
    
    NSMutableString *cdDisplay = [[NSMutableString alloc] initWithString:[ed eventDate]];
    [cdDisplay appendString: @"\n"];
    
    @try
    {
            [cdDisplay appendString: [ed eventSummary]];
    }
    @catch (NSException *exception)
    {
        
    }
    
    //Blows up here....

    

    cell.textLabel.text = cdDisplay;
    
    cell.textLabel.numberOfLines = 0;
    //[cell.textLabel sizeToFit];
    //cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tv willSelectRowAtIndexPath:(NSIndexPath *)path
{    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Create new thread to show activity indicator
    //[NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:nil];
    //[tableView deselectRowAtIndexPath:indexPath animated:NO];
    /*
    BibleVerseDetails *bvd = [tableData objectAtIndex:indexPath.row];
    
    NSString *scriptureUrlMobile = [[bvd scriptureUrl] stringByReplacingOccurrencesOfString:@"www" withString:@"mobile"];
    
    //Adding new view
    BibleVersesWebViewController* bibleVersesWebVC = [[BibleVersesWebViewController alloc] initWithUrl:scriptureUrlMobile];
    [self.navigationController pushViewController:bibleVersesWebVC animated:YES];
    //end
    [spinner stopAnimating];
    */
}

//Need to do this to ensure the full screen is covered upon orientation change.
/*
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    
    
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        NSLog(@"Landscape");
        [webUIView  setFrame: self.view.bounds];
    }
    
    if (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        NSLog(@"Portrait");
        [webUIView setFrame: self.view.bounds];
    }
    
    
}
 */

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


@end
