//
//  CalendarDetails.m
//  Oakwood
//
//  Created by The Osterkamps on 4/12/13.
//  Copyright (c) 2013 Oakwood Baptist Church. All rights reserved.
//

#import "CalendarDetails.h"

@implementation CalendarDetails

//@synthesize eventStartDate, eventEndDate, eventSummary, eventUrl, eventDescription;

+ (Boolean *)isValid:(CalendarDetails *)objForValidation
{
    Boolean valid = false;
    
    if (([objForValidation eventStartDate] != nil || [[objForValidation eventStartDate] isEqualToString:@""])
        &&
        ([objForValidation eventEndDate] != nil || [[objForValidation eventEndDate] isEqualToString:@""])
        &&
        ([objForValidation eventSummary] != nil || [[objForValidation eventSummary] isEqualToString:@""])
        &&
        ([objForValidation eventDescription] != nil || [[objForValidation eventDescription] isEqualToString:@""])
        )
    {
        valid = true;
    }
    
    return valid;
}

@end
