//
//  MyCustomAudioViewController.h
//  Oakwood
//
//  Created by The Osterkamps on 8/23/15.
//  Copyright (c) 2015 Oakwood Baptist Church. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCustomAudioViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIImageView *sermonImage;



@end
